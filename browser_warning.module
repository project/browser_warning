<?php
/**************************************************************************
* Hook Functions
*******************************************************************************/

/**
* hook_token_list
**/
function browser_warning_token_list($type = 'all') {
  $tokens['global']['browser'] = t("Currently viewing browser.");
  
  return $tokens;
}

/**
* hook_token_values
**/
function browser_warning_token_values($type, $object = NULL) {
  $values = array();
  switch ($type) {
    case 'global':
      $user_browser = browscap_get_browser();
      $user_browser_string = substr(trim($user_browser['parent']), 0, 255);
      if ($user_browser_string == '' or $user_browser_string == 'Default Browser') {
        $user_browser_string = trim($_SERVER['HTTP_USER_AGENT']);
      }
      
      $values['browser'] = $user_browser_string;
    break;
  }
  
  return $values;
}

/**
* hook_rules_condition_info
**/
function browser_warning_condition_info() {
  return array(
    'browser_warning_check_browser' => array(
      'label' => t('Browser Type'),
      'arguments' => array(
        'browser' => array('type' => 'string', 'label' => t('Browser to check')),
      ),
      'module' => 'System',
    ),
    'browser_warning_check_cookies' => array(
      'label' => t('Cookies are Enabled'),
      'module' => 'System',
    ),
    'browser_warning_check_https' => array(
      'label' => t('Accessed by HTTPS'),
      'module' => 'System',
    ),
    'browser_warning_check_javascript' => array(
      'label' => t('Javascript is Enabled'),
      'module' => 'System',
    ),
    'browser_warning_check_mobile' => array(
      'label' => t('Accessed by Mobile Device'),
      'module' => 'System',
    ),
    'browser_warning_check_crawler' => array(
      'label' => t('Accessed by Crawler'),
      'module' => 'System',
    ),
  );
}

/**
* Browser Check Callback
**/
function browser_warning_check_browser($browser) {
  $user_browser = browscap_get_browser();
  $user_browser_string = substr(trim($user_browser['parent']), 0, 255);
  if ($user_browser_string == '' or $user_browser_string == 'Default Browser') {
    $user_browser_string = trim($_SERVER['HTTP_USER_AGENT']);
  }
  
  if ($browser == $user_browser_string) {
    return TRUE;
  }
  return FALSE;
}

/**
* Browser Check Form
**/
function browser_warning_check_browser_form($settings, &$form) {
  $browser_options = array();
  $broswer_data = db_query("SELECT data FROM browscap");
  while ($data = unserialize(db_result($broswer_data))) {
    if ($data['parent'] != 'Default Browser' && $data['parent'] != '') {
      $browser_name = substr(trim($data['parent']), 0, 255);
      $browser_options[$browser_name] = $browser_name;
    }
  }
  asort($browser_options);
  
  $form['settings']['browser'] = array(
    '#type' => 'select',
    '#title' => t('Browser to check'),
    '#options' => $browser_options,
    '#default_value' => array($settings['browser']),
    '#description' => t('The browser to check for.'),
  );
}

/**
* Cookies Check Callback
**/
function browser_warning_check_cookies() {
  if (!empty($_COOKIE)) {
    return TRUE;
  }
  
  return FALSE;
}

/**
* HTTPS Check Callback
**/
function browser_warning_check_https() {
  if (!empty($_SERVER['HTTPS'])) {
    return TRUE;
  }
  
  return FALSE;
}

/**
* Javascript Check Callback
**/
function browser_warning_check_javascript() {
  if (empty($_COOKIE)) {
    // cookies are disabled, can't check
    return TRUE;
  }
  
  if (!empty($_COOKIE['has_js'])) {
    return TRUE;
  }
  
  return FALSE;
}

/**
* Mobile Check Callback
**/
function browser_warning_check_mobile() {
  $user_browser = browscap_get_browser();
  if ($user_browser['ismobiledevice']) {
    return TRUE;
  }
  
  return FALSE;
}

/**
* Crawler Check Callback
**/
function browser_warning_check_crawler() {
  $user_browser = browscap_get_browser();
  if ($user_browser['crawler']) {
    return TRUE;
  }
  
  return FALSE;
}